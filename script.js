// CONSTANTS AND GLOBAL VARIABLES
const CANVAS_WIDTH = window.innerWidth-10;
const CANVAS_HEIGHT = window.innerHeight-6;

var ENEMY_COUNT_BOUNCY;
var ENEMY_COUNT_NON_BOUNCY;

var ADD_NEW_ENEMY_INTERVAL = 1000;

var bestTime = localStorage.getItem("bestTime");
if (bestTime === null) {
    bestTime = 0;
}

var timerText;
var spaceshipGamePiece;
var enemies = [];

var startScreen= true;

// FUNCTIONS
function startGame(bouncy,non_bouncy,new_interval) {

    //SET GLOBAL VARIABLES FROM USER INPUT
    ENEMY_COUNT_BOUNCY = parseInt(bouncy);
    ENEMY_COUNT_NON_BOUNCY = parseInt(non_bouncy);
    ADD_NEW_ENEMY_INTERVAL = parseInt(new_interval);

    //CREAETE SPACESHIP, TIMER AND ENEMIES
    spaceshipGamePiece = new spaceship("red", CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2,50,50); 
    timerText = new timer();

    enemies.splice(0, enemies.length);
    
    // TWO TYPES OF ENEMIES: BOUNCY AND NON BOUNCY
    // BOUNCY ENEMIES BOUNCE OFF THE WALLS
    // NON BOUNCY ENEMIES DISSAPEAR AFTER THEY GO OUT OF BOUNDS, BUT A NEW ONE IS SPAWNED IN THEIR PLACE
    for (let i = 0; i < ENEMY_COUNT_BOUNCY+ENEMY_COUNT_NON_BOUNCY; i++) {
        if (i<ENEMY_COUNT_BOUNCY) {
            enemies.push(new enemy("gray","bouncy",));
        } else 
        {
            enemies.push(new enemy("#404040","non_bouncy"));
        }
    }

    myGameArea.initCanvas();

    // SHOW START SCREEN ON FIRST LAUNCH, START GAME AGAIN ON EVERY OTHER LAUNCH
    if (!startScreen) {
        myGameArea.start();
    } else {
        startGameScreen();
    }
}

function addNewEnemy () {
    
    // FUNCTION THAT PERIODICALLY ADDS NEW ENEMIES TO THE GAME
    if (Math.random() < 0.5) {
        enemies.push(new enemy("gray","bouncy"));
    } else {
        enemies.push(new enemy("#404040","non_bouncy"));
    }
}

var myGameArea = {
    canvas: document.createElement("canvas"),
    initCanvas: function () {
        // CREATE CANVAS AND ADD IT TO THE BODY
        this.canvas.id = "myGameCanvas";
        this.canvas.width = CANVAS_WIDTH;
        this.canvas.height = CANVAS_HEIGHT;
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
    },
    start: function () {
       
        // START GAME LOOP
        this.frameNo = 0;
        this.interval = setInterval(updateGameArea, 20);
        this.interval2 = setInterval(addNewEnemy, ADD_NEW_ENEMY_INTERVAL*1000);
        this.context.shadowColor = "white";
        this.context.shadowBlur = 20;
        this.context.font = "50px Arial";
    },
    stop: function () {
        // STOP GAME LOOP
        clearInterval(this.interval);
        clearInterval(this.interval2);
    },
    clear: function () {
        // CLEAR CANVAS
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    },
    // ISER INPUT HANDLING
    keys: [],
    keyDown: function (e) {
        myGameArea.keys[e.keyCode] = true;
        if (e.keyCode === 82) {
            //restart game
            myGameArea.stop();
            myGameArea.clear();
            startGame(ENEMY_COUNT_BOUNCY,ENEMY_COUNT_NON_BOUNCY,ADD_NEW_ENEMY_INTERVAL);
        }
        if (e.keyCode === 32 && startScreen) {
            //start game

            myGameArea.start();
            startScreen = false;
            playMusicOnLoad();
        }
    },
    keyUp: function (e) {
        myGameArea.keys[e.keyCode] = false;
    },
};

// EVENT LISTENERS FOR USER INPUT
window.addEventListener('keydown', myGameArea.keyDown);
window.addEventListener('keyup', myGameArea.keyUp);

class timer {

    update() {
        this.timerValue++; // INCREASE TIMER VALUE BY 1 EVERY 10 MILLISECONDS
    }

    constructor() {
        this.timerValue = 0; 
        // CAL THIS.UPDATE EVERY 10 MILLISECONDS
        this.interval = setInterval(this.update.bind(this), 10); 
    }

    show() {

        // SHOW TIMER AND BEST TIME ON THE CANVAS
        myGameArea.context.font = "35px Arial";
        myGameArea.context.fillStyle = "white";
        myGameArea.context.fillText("Vrijeme: "+milisecondsToTimeString(this.timerValue), CANVAS_WIDTH - 300, 50);

        if (this.timerValue > bestTime) { // CHECK IF THE TIMER IS BETTER THAN THE BEST TIME AND UPDATE IT IF IT IS
            bestTime = this.timerValue;
        }
        myGameArea.context.fillText("Najbolje vrijeme: "+milisecondsToTimeString(bestTime), CANVAS_WIDTH - 432, 100);

    }
}

function milisecondsToTimeString (miliseconds) { 

    // HELPER FUNCTION THAT CONVERTS MILLISECONDS TO A STRING IN THE FORMAT MM:SS:MS
    const minutes = Math.floor(miliseconds/6000);
    const seconds = Math.floor(miliseconds/100)%60  ;
    const milliseconds = miliseconds%100;

    return (minutes===0?"00":minutes<10?("0"+minutes):minutes)+":"+ (seconds===0?"00":seconds<10?("0"+seconds):seconds)+"."+(milliseconds<10?("0"+milliseconds):milliseconds);
}

function spaceship(color, x, y,width,height) {

    this.width = width;
    this.height = height;

    this.speed = 7.5;
  
    this.x = x;
    this.y = y;

    // DRAW SPACESHIP
    this.update = function () {
        ctx = myGameArea.context;
        ctx.save();
        ctx.translate(this.x + this.width / 2, this.y + this.height / 2); // Adjust the translation
        ctx.fillStyle = color;
        ctx.fillRect(-this.width / 2, -this.height / 2, this.width, this.height);
        ctx.restore();
    }

    // MOVE SPACESHIP
    this.newPos = function () {
        if (myGameArea.keys) { // MOVE PASED ON USER INPUT
            if (myGameArea.keys[37]) {
                // Right arrow key
               this.x -= this.speed;
            }
            if (myGameArea.keys[39]) {
                // Right arrow key
                this.x += this.speed;
            }
            if (myGameArea.keys[38]) {
                // Up arrow key
                this.y -= this.speed;
            }
            if (myGameArea.keys[40]) {
                // Down arrow key
                this.y += this.speed;
            }
        }

        // OUT OF BOUNDS HANDLING - SPACESHIP WILL APPEAR ON THE OTHER SIDE OF THE CANVAS
        if (this.x > CANVAS_WIDTH) {
            this.x = 0;
        } else if (this.x < 0) {
            this.x = CANVAS_WIDTH;
        }

        if (this.y > CANVAS_HEIGHT) {
            this.y = 0;
        } else if (this.y < 0) {
            this.y = CANVAS_HEIGHT;
        }
    }

}

function enemy(color,type) {

    this.outOfBounds = false;

    this.type = type

    this.size = (Math.random() *100)+40;
    
    // GENERATE RANDOM POSITION AND SPEED
    // ENEMIES CAN ONLY APPEAR OUTSIDE OF THE CANVAS 
    // AND THEY CANT MOVE TO THE SIDE THEY APPEARED FROM

    if (Math.random() < 0.5) {
      
        const generated_x = Math.random() * CANVAS_WIDTH;
        const generated_y = Math.random() < 0.5 ? -this.size : CANVAS_HEIGHT + this.size;
        
        this.x = generated_x;
        this.y = generated_y;
        if (generated_y < 0) {
            this.speed_y = (Math.random() * 9) + 4;
        }
        else {
            this.speed_y = (Math.random() * -9) - 4;
        }

        this.speed_x = (Math.random() * 13) - 6.5;

    } else {
    
        const generated_x = Math.random() < 0.5 ? -this.size : CANVAS_WIDTH + this.size;
        const generated_y = Math.random() * CANVAS_HEIGHT;
       
        this.x = generated_x;
        this.y = generated_y;

        if (generated_x < 0) {
            this.speed_x = (Math.random() * 9) + 4;
        }
        else {
            this.speed_x = (Math.random() * -9) - 4;
        }

        do {
            this.speed_y = (Math.random() * 13) - 6.5;
        }
        while (Math.abs(this.speed_y) < 1 )
    }
    
    // DRAW ENEMY
    this.update = function () {

        ctx = myGameArea.context;
        ctx.save();
        ctx.translate(this.x + this.size / 2, this.y + this.size / 2); // Adjust the translation
        ctx.fillStyle = color;
        ctx.fillRect(-this.size / 2, -this.size / 2, this.size, this.size);
        ctx.strokeStyle = "white";
        ctx.lineWidth = 2;
        ctx.strokeRect(-this.size / 2, -this.size / 2, this.size, this.size);
        ctx.restore();
    }

    // MOVE ENEMY
    this.newPos = function () {
        this.x += this.speed_x;
        this.y += this.speed_y;

        if (type === "non_bouncy") {
            // CHECK IF ENEMY IS OUT OF BOUNDS, AND SIGNAL THAT THEY ARE OUT OF BOUNDS
            if (this.x > CANVAS_WIDTH + this.size) {
                this.outOfBounds = true;
            } else if (this.x < 0 - this.size) {
                this.outOfBounds = true;
            }

            if (this.y > CANVAS_HEIGHT + this.size) {
                this.outOfBounds = true;
            } else if (this.y < 0 - this.size) {
                this.outOfBounds = true;
            }
        }
        else if (type==="bouncy") {
            // CHECK IF ENEMY IS OUT OF BOUNDS, AND THEN BOUNCE THEM OFF THE WALLS

            if (this.speed_x > 0 &&  this.x > CANVAS_WIDTH - this.size) {
                this.speed_x = -this.speed_x;
            } else if (this.speed_x < 0 && this.x < 0 ) {
                this.speed_x = -this.speed_x;
            }
        
            if (this.speed_y > 0 && this.y > CANVAS_HEIGHT - this.size) {
                this.speed_y = -this.speed_y;
            } else if (this.speed_y < 0 && this.y < 0) {
                this.speed_y = -this.speed_y;
            }
        }
    }

}

function checkCollision(spaceship, enemy) {

    // HELPER FUNCTION TO CHECK IF SPACESHIP AND ENEMY COLLIDE

    const spaceship_x = spaceship.x;
    const spaceship_y = spaceship.y;
    const spaceship_width = spaceship.width;
    const spaceship_height = spaceship.height;

    const enemy_x = enemy.x;
    const enemy_y = enemy.y;
    const enemy_size = enemy.size;

    if (spaceship_x < enemy_x + enemy_size &&
        spaceship_x + spaceship_width > enemy_x &&
        spaceship_y < enemy_y + enemy_size &&
        spaceship_y + spaceship_height > enemy_y) {
        return true;
    }
    return false;
}



function updateGameArea() {

    // FUNCTION THAT LOOPS EVERY 20 MILLISECONDS


    // CLEAR CANVAS TO PREPARE FOR NEW FRAME DRAWING
    myGameArea.clear();

    // DRAW SPACESHIP
    spaceshipGamePiece.newPos();
    spaceshipGamePiece.update();

    // DRAW ENEMIES
    enemiesCopy = enemies.slice();

    enemiesCopy.forEach(_enemy => {
        _enemy.newPos();
        _enemy.update();

        if (_enemy.outOfBounds) { 
            // IF ENEMY IS OUT OF BOUNDS, REMOVE IT FROM THE ARRAY AND SPAWN A NEW ONE IN ITS PLACE
            enemies.splice(enemies.indexOf(_enemy), 1);
            enemies.push(new enemy("#404040",_enemy.type));
        }
    });

    // DRAW TIMER
    timerText.show();

    // CHECK IF SPACESHIP COLLIDES WITH ANY OF THE ENEMIES
    enemies.forEach(_enemy => {
        if (checkCollision(spaceshipGamePiece, _enemy)) {
            myGameArea.stop();

            // PLAY IMPACT SOUND

            var audio = new Audio('sounds/impact.mp3');
            audio.volume = 0.5;
            audio.play();

            // GAME OVER TEXT
            myGameArea.context.font = "100px Arial";
            myGameArea.context.strokeStyle = "white";
            myGameArea.context.lineWidth = 3;
            myGameArea.context.strokeText("Game Over", CANVAS_WIDTH / 2 - 250, CANVAS_HEIGHT / 2);

            // RESTART TEXT
            myGameArea.context.font = "50px Arial";
            myGameArea.context.strokeStyle = "white";
            myGameArea.context.lineWidth = 1;
            myGameArea.context.strokeText("Press R to restart", CANVAS_WIDTH / 2 - 175, CANVAS_HEIGHT / 2 + 100);

            // CHECK IF LOCAL STORAGE IS UPDATED WITH BEST TIME
            if (localStorage.getItem("bestTime") === null || timerText.timerValue > localStorage.getItem("bestTime")) {
                localStorage.setItem("bestTime", timerText.timerValue);
            }

        }
    });

}

function startGameScreen() {

    // SHOW START SCREEN
    myGameArea.clear();
    myGameArea.context.font = "100px Arial";
    myGameArea.context.fillStyle = "white";
    myGameArea.context.fillText("Press space to start", CANVAS_WIDTH / 2 - 450, CANVAS_HEIGHT / 2 - 100);

    myGameArea.context.font = "50px Arial";
    myGameArea.context.strokeStyle = "white";
    myGameArea.context.lineWidth = 1;
    myGameArea.context.strokeText("Use arrow keys to move", CANVAS_WIDTH / 2 - 270, CANVAS_HEIGHT / 2 );

    myGameArea.context.font = "50px Arial";
    myGameArea.context.strokeStyle = "white";
    myGameArea.context.lineWidth = 1;
    myGameArea.context.strokeText("Press R to restart", CANVAS_WIDTH / 2 - 200, CANVAS_HEIGHT / 2 + 75);

    myGameArea.context.font = "50px Arial";
    myGameArea.context.fillStyle = "red";
    myGameArea.context.fillText("Best time: "+milisecondsToTimeString(bestTime), CANVAS_WIDTH / 2 - 215, CANVAS_HEIGHT / 2 + 300);
}

function playMusicOnLoad() {

    // PLAY BACKGROUND MUSIC ON GAME START
    var audio = new Audio('sounds/music.mp3');

    audio.addEventListener('ended', function () {
        // LOOP MUSIC
        this.currentTime = 0;
        this.play();
    }
    , false);

    // SET VOLUME TO 50%
    audio.volume = 0.5;

    audio.play();
}

//startGame();